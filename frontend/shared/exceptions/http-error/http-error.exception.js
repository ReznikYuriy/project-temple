"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpError = void 0;
const enums_1 = require("../../common/enums");
const constants_1 = require("./common/constants");
class HttpError extends Error {
    constructor({ status = enums_1.HttpCode.INTERNAL_SERVER_ERROR, message = constants_1.DEFAULT_MESSAGE, } = {}) {
        super(message);
        this.status = status;
        this.name = enums_1.CustomExceptionName.HTTP_ERROR;
    }
}
exports.HttpError = HttpError;
