import { HttpCode } from '../../common/enums';
declare class HttpError extends Error {
    status: HttpCode;
    constructor({ status, message, }?: {
        status?: HttpCode | undefined;
        message?: string | undefined;
    });
}
export { HttpError };
