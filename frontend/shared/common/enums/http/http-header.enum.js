"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpHeader = void 0;
var HttpHeader;
(function (HttpHeader) {
    HttpHeader["CONTENT_TYPE"] = "content-type";
})(HttpHeader || (HttpHeader = {}));
exports.HttpHeader = HttpHeader;
