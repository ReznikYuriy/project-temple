import React, { useState } from "react";
import './signinpage.css';

const SignInPage: React.FC = () => {
   const [email, setEmail] = useState <string> ('');
   const [password, setPassword] = useState <string> ('');
   const [emailError, setEmailError] = useState<boolean>(false);
   const [passwordError, setPasswordError] = useState<boolean>(false);
   
   const handleChangeEmail = (event: any): void => {
      event.preventDefault();
      setEmail(event.target.value);
   }
   const handleChangePassword = (event: any): void => {
      event.preventDefault();
      setPassword(event.target.value);
   }
   const Regex = RegExp(/^\s?[A-Z0–9]+[A-Z0–9._+-]{0,}@[A-Z0–9._+-]+\.[A-Z0–9]{2,4}\s?$/i);

   const handleSubmit = (event: any): void => {
      event.preventDefault();
      (Regex.test(email)) ? setEmailError(false) : setEmailError(true);
      (password.length < 6) ? setPasswordError(true) : setPasswordError(false);
   }
   return (
      <div className='wrapper'>
         <div className='form-wrapper'>
            <h2>Sign In</h2>
            <form onSubmit={handleSubmit} noValidate >
               <div className='email'>
                  <label htmlFor="email">Email</label>
                  <input type='email' name='email' onChange={handleChangeEmail} />
                  {emailError &&  <span className='error-span'>Email is not valid!</span>}
               </div>
               <div className='password'>
                  <label htmlFor="password">Password</label>
                  <input type='password' name='password' onChange={handleChangePassword} />
                  {passwordError &&  <span className='error-span'>Password must be six or more characters long!</span>}
               </div>
               <div className='submit'>
                  <button>Sign In</button>
               </div>
            </form>
         </div>
      </div>
   );
}
export default SignInPage