"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContentType = void 0;
var ContentType;
(function (ContentType) {
    ContentType["JSON"] = "application/json";
})(ContentType || (ContentType = {}));
exports.ContentType = ContentType;
