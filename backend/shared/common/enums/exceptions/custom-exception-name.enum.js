"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomExceptionName = void 0;
var CustomExceptionName;
(function (CustomExceptionName) {
    CustomExceptionName["HTTP_ERROR"] = "HttpError";
})(CustomExceptionName || (CustomExceptionName = {}));
exports.CustomExceptionName = CustomExceptionName;
