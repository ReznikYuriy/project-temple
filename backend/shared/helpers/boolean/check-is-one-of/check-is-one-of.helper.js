"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkIsOneOf = void 0;
const checkIsOneOf = (checkItem, ...checksItems) => {
    return checksItems.some((item) => item === checkItem);
};
exports.checkIsOneOf = checkIsOneOf;
