declare const checkIsOneOf: <T>(checkItem: T, ...checksItems: T[]) => boolean;
export { checkIsOneOf };
